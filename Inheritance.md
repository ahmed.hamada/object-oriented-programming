## Inheritance

allows a class (subclass) to inherit properties and methods from another class (superclass). In JavaScript, inheritance is achieved through prototype chaining.

## Real-Life Example: Animal Hierarchy

Consider a hierarchy of animals:

- **Animal**: Represents the most general characteristics and behaviors shared by all animals.
- **Mammal**: A subclass of Animal, with additional characteristics specific to mammals.
- **Bird**: Another subclass of Animal, representing characteristics unique to birds.
- **Dog**: A subclass of Mammal, inheriting from both Animal and Mammal.

### Implementation

```javascript
// Define the Animal class
class Animal {
  constructor(species) {
    this.species = species;
  }

  eat() {
    console.log(`${this.species} is eating`);
  }

  sleep() {
    console.log(`${this.species} is sleeping`);
  }
}

// Define the Mammal class inheriting from Animal
class Mammal extends Animal {
  giveBirth() {
    console.log(`${this.species} is giving birth`);
  }
}

// Define the Bird class inheriting from Animal
class Bird extends Animal {
  fly() {
    console.log(`${this.species} is flying`);
  }
}

// Define the Dog class inheriting from Mammal
class Dog extends Mammal {
  constructor(species, breed) {
    super(species);
    this.breed = breed;
  }

  bark() {
    console.log(`${this.species} (${this.breed}) is barking`);
  }
}

// Create instances and demonstrate inheritance
const cat = new Mammal('Cat');
cat.eat(); // Output: Cat is eating
cat.giveBirth(); // Output: Cat is giving birth

const sparrow = new Bird('Sparrow');
sparrow.sleep(); // Output: Sparrow is sleeping
sparrow.fly(); // Output: Sparrow is flying

const goldenRetriever = new Dog('Dog', 'Golden Retriever');
goldenRetriever.eat(); // Output: Dog is eating
goldenRetriever.bark(); // Output: Dog (Golden Retriever) is barking
