# Principles of Object-Oriented Programming (OOP)

- **Encapsulation**: Bundling data (attributes) and methods (functions) that operate on the data into a single unit, known as a class. It hides the internal state of an object and only exposes the necessary functionalities.
- **Inheritance**: The mechanism by which a class can inherit properties and behavior from another class, promoting code reuse and establishing a hierarchical relationship between classes.
- **Polymorphism**: The ability for objects of different classes to be treated as objects of a common superclass. It allows methods to be defined in the superclass and overridden by subclasses.
- **Abstraction**: Reducing complexity by representing essential features without including the background details. It allows focusing on the relevant aspects of an object while hiding irrelevant details.

# Primitive Data Types

Primitive data types are the basic data types provided by a programming language. They are typically built-in and represent simple values. Examples include integers, floating-point numbers, characters, and booleans.

# Why Group Variables Together

Grouping variables together is essential for organizing related data and operations into coherent units. This promotes code clarity, maintainability, and reusability. By encapsulating variables within a class or structure, you can control access to them and ensure they are manipulated consistently.

# Structure

- **Struct**: In programming, a structure (struct) is a composite data type that groups together related variables under one name. Unlike classes in object-oriented programming, structures typically do not have methods associated with them.
- **Struct vs Array**: While both structures and arrays are ways of organizing data, they serve different purposes. Arrays are used to store homogeneous data types in a contiguous memory block, accessed by indexing. Structures, on the other hand, allow you to group variables of different data types together to represent a single entity.

# The Problem with Structures

One issue with structures is the lack of encapsulation. In many programming languages, structures do not support data hiding or methods, which means that the internal representation of the structure can be directly accessed and modified by any part of the program. This can lead to unintended changes and makes it harder to maintain and reason about the code.

# Object and Class

- **Object**: An object is an instance of a class. It represents a specific instance of a particular class, possessing its own state (attributes) and behavior (methods).
- **Class**: A class is a blueprint or template for creating objects. It defines the properties and behaviors common to all objects of that type. It encapsulates data for the object and provides methods to operate on that data.
