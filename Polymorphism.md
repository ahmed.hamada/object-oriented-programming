# Polymorphism

allows objects of different classes to be treated as objects of a common superclass, providing a way to abstract behaviors across different types of objects.

## Compile-time Polymorphism (Method Overloading)

- Method overloading allows multiple methods in the same class to have the same name but different parameters.
- The appropriate method to execute is determined at compile-time based on the number and types of arguments passed.

## Runtime Polymorphism (Method Overriding)

- Method overriding allows a subclass to provide a specific implementation of a method that is already defined in its superclass.
- The appropriate method to execute is determined at runtime based on the actual object type.

## Real-life Example

Consider a `Shape` superclass with a method `calculateArea()`. This method is overridden by subclasses such as `Rectangle`, `Circle`, and `Triangle`, each providing its own implementation of `calculateArea()` based on its specific shape formula.

```java
// Shape superclass
class Shape {
    public double calculateArea() {
        return 0; // Default implementation (for general shapes)
    }
}

// Rectangle subclass
class Rectangle extends Shape {
    private double length;
    private double width;

    // Constructor and other methods...

    @Override
    public double calculateArea() {
        return length * width;
    }
}

// Circle subclass
class Circle extends Shape {
    private double radius;

    // Constructor and other methods...

    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }
}

// Triangle subclass
class Triangle extends Shape {
    private double base;
    private double height;

    // Constructor and other methods...

    @Override
    public double calculateArea() {
        return 0.5 * base * height;
    }
}
